import java.util.List;

class Liste{
    private int val;
    private Liste suiv;
    
    //attention : utilisez plutôt "getVal()" que "val", car getVal() renvoie une exception quand la liste est vide,
	//et vous empêche donc d'utiliser la valeur "fantôme" du dernier maillon

    //liste vide =_def (*,null)
 
    //////////////////////////////////////////////
    //////// méthodes fournies
    //////////////////////////////////////////////
    
    public Liste(){
	suiv = null;
    }

    public Liste(Liste l){
	if(l.estVide()){
	    suiv=null;
	}
	else{
	    val = l.val;
	    suiv = new Liste(l.suiv);
	}
	}

    public Liste(int x, Liste l){
	val = x;
	suiv = new Liste(l);
    }

	public int getVal(){
		//sur liste non vide
		if(estVide())
			throw new RuntimeException("getVal appelée sur liste vide");
		return val;
	}

	public int longeur() {
		if(this.estVide()) {
			return 0;
		}
		return 1 + this.suiv.longeur();
	}

	public int somme() {
		if(this.estVide()) {
			return 0;
		}
		return this.val + this.suiv.somme();
	}

	public boolean estCroissant() {
		if(this.estVide()) {
			System.out.println("caca");
			return true;
		}
		else if(this.suiv.estVide()) {
			return true;
		}
		else if(this.val > this.suiv.val) {
			return false;
		}
		return this.suiv.estCroissant();
	}

	public int get(int i) {
		if(this.estVide()) {
			throw new RuntimeException("get appelée sur liste vide");
		}
		else if(i == 0) {
			return this.val;
		}
		return this.suiv.get(i - 1);
	}

	public Liste getSuiv(){
		return suiv;
	}

	public void ajoutTete(int x){
		if(estVide()){
			val = x;
			suiv = new Liste();
		}
		else {
			Liste aux = new Liste();
			aux.val = getVal();
			aux.suiv = suiv;
			val = x;
			suiv = aux;
		}
	}

	public void ajoutFin(int x){
		if (estVide()) {
			val = x;
			suiv = new Liste();
		}
		else {
			this.suiv.ajoutFin(x);
		}
	}

	public void concat(Liste l) {
		// on dois concatener a partir de la tete car si la tete cahnhe le reste de la liste change
		if (estVide()) {
			this.val = l.val;
		}
		else {
			this.suiv.concat(l);
		}
		this.suiv = l;
	}

	public Liste supprOccs(int x) {
		Liste aux = new Liste();

		if (estVide()) {
			return aux;
		}
		else if(this.val == x) {
			return this.suiv.supprOccs(x);
		}
		else {
			aux.val = this.val;
			aux.suiv = this.suiv.supprOccs(x);
			return aux;
		}
	}

	public void echange(int i, int j) {
		if (estVide()) {
			return;
		}
		else if(i == 0) {
			int aux = this.val;
			this.val = this.get(j);
			this.suiv.echange(0, j - 1);
			this.suiv.val = aux;
		}
		else {
			this.suiv.echange(i + 1, j - 1);
		}
	}

	public Liste retourne() {
		if (estVide()) {
			return this;
		}
		else {
			// on utilise echange
			this.echange(0, this.longeur() - 1);
			this.suiv.retourne();
		}
		return this;
	}

	//Ecrire une méthode Liste supprOccsV2(int x) qui retourne une liste en supprimant toutes les occurences de
	//x de this, et ne crée pas de nouveau maillon.

	public Liste supprOccsV2(int x) {
		if (estVide()) {
			return this;
		}
		else if(this.val == x) {
			return this.suiv.supprOccsV2(x);
		}
		else {
			this.suiv = this.suiv.supprOccsV2(x);
			return this;
		}
	}
    public void supprimeTete(){
		//sur liste non vide
		if(suiv.estVide()){
			suiv = null;
		}
		else {
			val = suiv.getVal();
			suiv = suiv.suiv;
		}
    }

    public boolean estVide(){
	return suiv==null;
    }



    public String toString(){
	if(estVide()){
	    return "()";
	}
	else{
	    return getVal()+" "+suiv.toString();
	}
    }



    //////////////////////////////////////////////
    //////// méthodes du TD
    //////////////////////////////////////////////
    
    

    public static void main(String[] arg){
	Liste caca = new Liste();
	caca.ajoutTete(4);
	caca.ajoutTete(3);
	caca.ajoutTete(2);
	caca.ajoutTete(18);


		//System.out.println(l.retourne());


	Liste l = new Liste();
	l.ajoutTete(1);

	Liste l2 = new Liste();
	l2.ajoutTete(2);
	l2.ajoutTete(3);



	//l2.echange(0, 1);
		System.out.println("Liste pas retourner : " +caca);
		System.out.println("Liste retourner : "+caca.retourne());

	/*l.concat(l2);
	System.out.println(l);
	l2.val = 50;
		System.out.println("l2 : "+l2);
	System.out.println(l);
	l2.suiv.val = 51;
		System.out.println("l2 : " + l2);
	System.out.println(l);*/


		//System.out.println(l2.supprOccsV2(3));
	/*l.ajoutFin(5);
	System.out.println(l.longeur());
	System.out.println(l.somme());
	System.out.println(l.estCroissant());
	System.out.println(l.get(0));
	System.out.println("l : " + l);*/
	/*l.supprimeTete();
	System.out.println("l : " + l);
	l.supprimeTete();
	System.out.println("l : " + l);
	l.supprimeTete();
	System.out.println("l : " + l);*/


	}
}
